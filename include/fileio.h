#ifndef FILEIO_H
#define FILEIO_H

#include <QObject>
#include <QFile>
#include <QTextStream>
#include <iostream>

class FileIO : public QObject
{
    Q_OBJECT

private:
    QString path;

public slots:
    bool write(const QString& filename, const QString& data)
    {

//std::cerr << (path + filename).toStdString() << std::endl;

        if (filename.isEmpty())
            return false;

        QFile file(path + QString("/") + filename);
        if (!file.open(QFile::WriteOnly | QFile::Truncate))
            return false;

        QTextStream out(&file);
        out << data;
        file.close();
        return true;
    }

    bool remove(const QString& filename)
    {
        if (filename.isEmpty())
            return false;

        return QFile::remove(path + QString("/") + filename);
    }

    bool rename(const QString& filename, const QString& newname)
    {
        if (filename.isEmpty() || newname.isEmpty())
            return false;

        return QFile::rename(path + QString("/") + filename, path + QString("/") + newname);
    }


public:
    FileIO(QString p) {
        path = p;
    }
};

#endif // FILEIO_H
