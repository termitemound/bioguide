TEMPLATE = app
TARGET = bioguide
android:TARGET = BioGuide

VERSION = 1.0.0

OBJECTS_DIR = build
MOC_DIR = $${OBJECTS_DIR}/moc
RCC_DIR = $${OBJECTS_DIR}/qrc

QT += core qml quick webview

INCLUDEPATH += include

HEADERS += include/fileio.h

SOURCES += src/bioguide.cpp

RESOURCES += bioguide.qrc

DISTFILES += \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/gradle.properties \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/res/values/libs.xml \
    src/bioguide.qml \
    LICENSE \
    README.md

DISTDIRS = android www data

DISTDIR_GZ = $${OBJECTS_DIR}/$${TARGET}-$${VERSION}


unix:QMAKE_EXTRA_TARGETS += dist_gz

dist_gz.commands = \
    @echo "Creating tarball: $${TARGET}-$${VERSION}.tar.gz"; \
    rm -rf $${DISTDIR_GZ}; \
    mkdir -p $${DISTDIR_GZ}; \
    cp -f --parents $${SOURCES} $${HEADERS} $${RESOURCES} $${TARGET}.spec $${TARGET}.pro $${TARGET}.pro.user $${DISTFILES} $${DISTDIR_GZ}; \
    cp -f -r --parent $${DISTDIRS} $${DISTDIR_GZ}; \
    (cd $${OBJECTS_DIR} && tar -cz $${TARGET}-$${VERSION} > ../$${TARGET}-$${VERSION}.tar.gz); \
    rm -rf $${DISTDIR_GZ}; \

target.path = /usr/bin
data.path = /usr/share/$${TARGET}
data.files = data/*

INSTALLS += target data

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
