/*!
\file
\brief js-файл с телами функций

Данный файл содержит в себе тела функций,
которые вызываются в связи с событиями на html-странице
*/



/* имя файла текущего определителя */
var bg_file = null;

/* текущий определитель в виде DOM-объекта (создан из xml-файла) */
var bg_guide;

/* список коллекций в виде DOM-объекта (создан из файла collections.xml) */
var bg_collections;

/* номер текущей коллекции */
var bg_collection_num;

/* имя текущей коллекции */
var bg_collection_title;

/* экземпляр класса XSLTProcessor */
var xsltProcessor = null;

/* название текущего определителя */
var bg_title = null;

/* массив пар значений (taxon_id, record_number); каждая пара соответствует выбранной пользователем тезе определителя */
var bg_history = [];

/* индекс в массиве bg_history[], указывающий на текущую отображаемую тезу */
var bg_history_index;

/* экземпляр класса XPathEvaluator */
var xpathEvaluator = null;

/* список уже загруженных определителей в виде DOM-объекта (создан из локального файла bioguide.xml) */
var bg_list = null;

/* экземпляр класса DOMParser */
var parser = null;

var xmlSer = null;

/* список определителей, хранящихся на сервере, в виде DOM-объекта (создан из файла bioguide.xml, загруженного с сервера) */
var bg_list_server;

/* URL хранящихся на сервере файлов определителей и списка определителей (bioguide.xml) */
//var bg_url_base = "http://192.168.1.72/bioguide/guides";
var bg_url_base = "http://termitemound.gitlab.io/bioguide/data";

/*имя файла, с которым нужно совершить файловую опрерацию*/
var qml_filename = "";

/*содержимое перезаписываемого файла*/
var qml_data = "";

/*список файлов с которыми нужно совершить файловые опрерации*/
var qml_filelist = [];

/*массив текстового содержимого определителей*/
var qml_guides = [];

/*идентификатор таймера, используемого для переодической проверки ответов на запрос в qml*/
var timeout_id;

/*массив объектов типа XMLHttpRequest*/
var axhr = [];

/*счётчик элементов массива axhr[]*/
var axhr_count = 0;


/**
 * Подготавливает глобальные переменные для дальнейшей работы с ними<br/>
 * и вызывает функцию bg_start()<br/>
*/
function bg_setup() {
    xsltProcessor = new XSLTProcessor();
    let bg_xsl = loadXML("bioguide.xsl");
    xsltProcessor.importStylesheet(bg_xsl);
    xpathEvaluator = new XPathEvaluator();
    bg_list = loadXML("bioguide.xml");
    bg_collections = loadXML("collections.xml");
    parser = new DOMParser();
    xmlSer = new XMLSerializer();

//console.error(document.body.p);

    bg_start();
}


/**
 * Загружает xml с помощью механизма XMLHttpRequest<br/>
 * (реализуется одноимённым классом) синхронно
 * @return фрагмент xml в виде текста
*/
function loadXMLtext(filename) {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", filename, false);
    xhr.send();

    return xhr.responseText;
}


/**
 * Загружает xml с помощью механизма XMLHttpRequest<br/>
 * (реализуется одноимённым классом) синхронно
 * @return xml в виде DOM объекта
*/
function loadXML(filename) {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", filename, false);
    xhr.send();

    return xhr.responseXML;
}


/**
 * Загружает xml с помощью механизма XMLHttpRequest<br/>
 * (реализуется одноимённым классом) асинхронно;<br/>
 * вызывает функцию обработчик события (конца загрузки)
*/
function loadXMLAsync(filename, afterLoad) {
    axhr.push(new XMLHttpRequest());
    axhr[axhr.length - 1].addEventListener('loadend', afterLoad);
    axhr[axhr.length - 1].open("GET", filename);
    axhr[axhr.length - 1].send();
}


/**
 * Заполняет html-страницу начальным содержимым<br/>
 * (корневая логическая страница)<br/>
*/
function bg_start() {
    let p_top = loadXMLtext("bg_index_top.html");
    let p_mid = loadXMLtext("bg_index_mid.html");
    let p_bot = loadXMLtext("bg_index_bot.html");

    document.getElementById("page_top").innerHTML = p_top;
    document.getElementById("page_mid").innerHTML = p_mid;
    document.getElementById("page_bot").innerHTML = p_bot;
}


/**
 * Заполняет html-страницу новым содержимым<br/>
 * (логическая страница 'Список определителей')<br/>
*/
function bg_guidelist() {
    let p_top = loadXMLtext("bg_list_top.html");
    let p_bot = loadXMLtext("bg_list_bot.html");

    document.getElementById("page_top").innerHTML = p_top;
    document.getElementById("page_bot").innerHTML = p_bot;

    if (bg_list.children[0].childElementCount == 0) {
        document.getElementById("btn_remove").setAttribute("disabled", "disabled");
    }

    xsltProcessor.setParameter(null, "action", "show_guide_list");

    let fragment = xsltProcessor.transformToFragment(bg_list, document);

    document.getElementById("page_mid").textContent = "";
    document.getElementById("page_mid").appendChild(fragment);
}

/**
 * Обрабатывает запрос (OnClick) на удаление определителей:<br/>
 * обновление содержимого bioguide.xml (файл со списком определителей)<br/>
 * перерисовка страницы<br/>
 * передача запроса на удаление в qml<br/>
 * запуск таймера, по окончании кторого вызывается remove_guides() (проверка ответа)<br/>
*/
function bg_guide_remove() {
    let i, count = 0, confirm;

    for (i = 0; i < bg_list.children[0].childElementCount; i++) {
        if (document.forms['guides'].elements[i].checked != "") {
            count++;
        }
    }
    if (count == 0) {
        return;
    }

    confirm = window.confirm("Удалить выбранные определители (" + count + ")?");

    if (confirm == 0) {
        return;
    }

    document.getElementById("sp_label").innerHTML = "Удаление...";
    document.getElementById("spinner").style.visibility = "visible";

    for (i = bg_list.children[0].childElementCount - 1; i >= 0; i--) {
        if (document.forms['guides'].elements[i].checked != "") {
            qml_filelist.push(bg_list.children[0].children[i].getAttribute("file"));
            bg_list.children[0].children[i].remove();
        }
    }

    qml_filename = "bioguide.xml";
    qml_data = xmlSer.serializeToString(bg_list);
    document.getElementById("page_req").innerHTML = "update";

    timeout_id = setTimeout(remove_guides, 250);
}

/**
 * проверка и обработка ответа на запрос в qml на удаление определителей
*/
function remove_guides() {
    if (document.getElementById("page_req").innerHTML != "") {
        timeout_id = setTimeout(remove_guides, 250);
        return;
    }

    if (qml_filelist.length == 0) {
        document.getElementById("spinner").style.visibility = "hidden";
        bg_guidelist();
        return;
    }

    qml_filename = qml_filelist.pop();
    document.getElementById("page_req").innerHTML = "remove";

    timeout_id = setTimeout(remove_guides, 250);
}


/**
 * Вызывается по событию loadend при загрузке списка серверных определителей.<br/>
 * Записывает список серверных определителей в виде DOM объекта в переменную bg_list_serve<br/>
 * и вызывает bg_guide_add_cont() - формирование середины страницы.
 * @param {event} evt - объект описатель события, передаётся браузером
*/
function bg_guide_add_callback(evt) {
    bg_list_server = axhr[0].responseXML;
    bg_guide_add_cont();
}


/**
 * Отрисовка новой логической страницы<br/>
 * ('Добавление определителей')<br/>
*/
function bg_guide_add() {
    let p_top = loadXMLtext("bg_guide_add_top.html");
    let p_bot = loadXMLtext("bg_guide_add_bot.html");

    document.getElementById("page_top").innerHTML = p_top;
    document.getElementById("page_bot").innerHTML = p_bot;

    document.getElementById("sp_label").innerHTML = "Загрузка...";
    document.getElementById("spinner").style.visibility = "visible";

    axhr = [];

    loadXMLAsync(bg_url_base + "/bioguide.xml", bg_guide_add_callback);
}


/**
 * отрисовка середины логической страницы 'Добавление определителей' ('Список серверных определителей')
*/
function bg_guide_add_cont() {
    axhr = [];

    let union_lists = '\
<?xml version="1.0" encoding="utf-8"?>\n\
<union>\n\
</union>';
    let doc = parser.parseFromString(union_lists, "application/xml");
    doc.children[0].append(bg_list.children[0].cloneNode(true));
    doc.children[0].append(bg_list_server.children[0].cloneNode(true));

    xsltProcessor.setParameter(null, "action", "show_guide_list_server");

    let fragment = xsltProcessor.transformToFragment(doc, document);

    document.getElementById("page_mid").textContent = "";
    document.getElementById("page_mid").appendChild(fragment);

    document.getElementById("spinner").style.visibility = "hidden";
}


/**
 * Вызывается по событию loadend при загрузке определителя.<br/>
 * Добавляет тело скачиваемого определителя в массив qml_guides,<br/>
 * переставляет счётчик элементов массива и вызывает bg_guide_add_confirm_cont(), если все определители записаны в массив
 * @param {event} evt - объект описатель события, передаётся браузером
*/
function bg_guide_add_confirm_callback(evt) {
    qml_guides.push(evt.currentTarget.responseText);
    axhr_count++;
    if (axhr_count == axhr.length) {
        bg_guide_add_confirm_cont();
    }
}


/**
 * Обрабатывает запрос (OnClick) на добавление определителя:<br/>
 * обновление содержимого bg_list (DOM-объект со списком определителей)<br/>
 * перерисовка страницы<br/>
 * передача запроса на добавление определителя в qml<br/>
 * запуск таймера, по окончании кторого вызывается update_collections() (проверка ответа)<br/>
*/
function bg_guide_add_confirm() {
    let count = 0, confirm;
    let i, j;

    for (i = 0; i < document.forms['guides_server'].length; i++) {
        if (document.forms['guides_server'].elements[i].checked != "") {
            count++;
        }
    }
    if (count == 0) {
        return;
    }

    confirm = window.confirm("Загрузить выбранные определители (" + count + ")?");

    if (confirm == 0) {
        return;
    }

    document.getElementById("sp_label").innerHTML = "Загрузка...";
    document.getElementById("spinner").style.visibility = "visible";

    axhr = [];
    axhr_count = 0;

    for (i = 0; i < document.forms['guides_server'].length; i++) {
        if (document.forms['guides_server'].elements[i].checked == "") {
            continue;
        }
        for (j = 0; j < bg_list.children[0].childElementCount; j++) {
            if (bg_list.children[0].children[j].getAttribute("file") == document.forms['guides_server'].elements[i].value) {
                loadXMLAsync(bg_url_base + "/" + bg_list.children[0].children[j].getAttribute("file"), bg_guide_add_confirm_callback);
                qml_filelist.push(bg_list.children[0].children[j].getAttribute("file"));
                bg_list.children[0].children[j].remove();
            }


        }
        for (j = 0; j < bg_list_server.children[0].childElementCount; j++) {
            if (bg_list_server.children[0].children[j].getAttribute("file") == document.forms['guides_server'].elements[i].value) {
                loadXMLAsync(bg_url_base + "/" + bg_list_server.children[0].children[j].getAttribute("file"), bg_guide_add_confirm_callback);
                qml_filelist.push(bg_list_server.children[0].children[j].getAttribute("file"));
                bg_list.children[0].append(bg_list_server.children[0].children[j]);
            }
        }
    }
}


/**
 * отправка запроса на сохранение определителей, запуск таймера
*/
function bg_guide_add_confirm_cont() {
    axhr = [];
    axhr_count = 0;

    qml_filename = qml_filelist.pop();
    qml_data = qml_guides.pop();
    document.getElementById("page_req").innerHTML = "save";

    timeout_id = setTimeout(save_guides, 250);
}


/**
 * проверка и обработка ответа на запрос в qml на добавление определителей
*/
function save_guides() {
    if (document.getElementById("page_req").innerHTML != "") {
        timeout_id = setTimeout(save_guides, 250);
        return;
    }

    if (qml_filelist.length == 0) {
        if (qml_filename == "bioguide.xml") {
            document.getElementById("spinner").style.visibility = "hidden";
            bg_guidelist();
            return;
        }

        qml_data = xmlSer.serializeToString(bg_list);
        qml_filename = "bioguide.xml";
        document.getElementById("page_req").innerHTML = "update";

        timeout_id = setTimeout(save_guides, 250);
        return;
    }

    qml_filename = qml_filelist.pop();
    qml_data = qml_guides.pop();
    document.getElementById("page_req").innerHTML = "save";

    timeout_id = setTimeout(save_guides, 250);
}


/**
 * Отрисовка новой логической страницы<br/>
 * ('Список коллекций')<br/>
*/
function bg_collections_list() {
    let p_top = loadXMLtext("bg_collections_top.html");
    let p_bot = loadXMLtext("bg_collections_bot.html");

    document.getElementById("page_top").innerHTML = p_top;
    document.getElementById("page_bot").innerHTML = p_bot;

    if (bg_collections.children[0].childElementCount == 0) {
        document.getElementById("btn_remove").setAttribute("disabled", "disabled");
    }

    xsltProcessor.setParameter(null, "action", "show_collections_list");

    let fragment = xsltProcessor.transformToFragment(bg_collections, document);

    document.getElementById("page_mid").textContent = "";
    document.getElementById("page_mid").appendChild(fragment);
}


/**
 * Отрисовка новой логической страницы<br/>
 * ('Добавление коллекции')<br/>
*/
function bg_collection_add() {
    let p_top = loadXMLtext("bg_collection_add_top.html");
    let p_mid = loadXMLtext("bg_collection_add_mid.html");
    let p_bot = loadXMLtext("bg_collection_add_bot.html");

    document.getElementById("page_top").innerHTML = p_top;
    document.getElementById("page_mid").innerHTML = p_mid;
    document.getElementById("page_bot").innerHTML = p_bot;
}



/**
 * Обрабатывает запрос (OnClick) на добавление коллекции:<br/>
 * обновление содержимого bg_collections (DOM-объект со списком коллекций)<br/>
 * перерисовка страницы<br/>
 * передача запроса на обновление collections.xml в qml<br/>
 * запуск таймера, по окончании кторого вызывается update_collections() (проверка ответа)<br/>
*/
function bg_collection_add_confirm() {
    let collection_name = document.getElementById("col_name").value;
    let i;

    for (i = 0; i < bg_collections.children[0].childElementCount; i++) {
        if (bg_collections.children[0].children[i].children[0].innerHTML == collection_name) {
            window.alert('Коллекция с именем "' + collection_name + '" уже существует');
            return;
        }
    }

    document.getElementById("sp_label").innerHTML = "Добавление...";
    document.getElementById("spinner").style.visibility = "visible";

    collection = '\n\
    <collection>\n\
	<name>' + collection_name + '</name>\n\
	<items>\n\
	</items>\n\
    </collection>\n';

    let doc = parser.parseFromString(collection, "application/xml");

    bg_collections.children[0].append(doc.children[0]);

    qml_data = xmlSer.serializeToString(bg_collections);
    qml_filename = "collections.xml";
    document.getElementById("page_req").innerHTML = "update";

    timeout_id = setTimeout(update_collections, 250);
}


/**
 * проверка и обработка ответа на запрос в qml на обновление коллекций<br/>
 * (при вызове по окончании таймера в функции bg_collection_add_confirm() или bg_collection_remove())<br/>
*/
function update_collections() {
    if (document.getElementById("page_req").innerHTML != "") {
        timeout_id = setTimeout(update_collections, 250);
        return;
    }

    document.getElementById("spinner").style.visibility = "hidden";

    bg_collections_list();
}


/**
 * проверка и обработка ответа на запрос в qml на обновление коллекций<br/>
 * (при вызове по окончании таймера в функции ...())<br/>
*/
function update_collections2() {
    if (document.getElementById("page_req").innerHTML != "") {
        timeout_id = setTimeout(update_collections2, 250);
        return;
    }

    document.getElementById("spinner").style.visibility = "hidden";

    bg_tablelist();
}


/**
 * Обрабатывает запрос (OnClick) на удаление коллекции:<br/>
 * обновление содержимого bg_collections (DOM-объект со списком коллекций)<br/>
 * перерисовка страницы<br/>
 * передача запроса на обновление collections.xml в qml<br/>
 * запуск таймера, по окончании кторого вызывается update_collections() (проверка ответа)<br/>
*/
function bg_collection_remove() {
    let i, count = 0, confirm;

    for (i = 0; i < bg_collections.children[0].childElementCount; i++) {
        if (document.forms['collections'].elements[i].checked != "") {
            count++;
        }
    }
    if (count == 0) {
        return;
    }

    confirm = window.confirm("Удалить выбранные коллекции (" + count + ") с их содержимым?");

    if (confirm == 0) {
        return;
    }

    document.getElementById("sp_label").innerHTML = "Удаление...";
    document.getElementById("spinner").style.visibility = "visible";

    for (i = bg_collections.children[0].childElementCount - 1; i >= 0; i--) {
        if (document.forms['collections'].elements[i].checked != "") {
            bg_collections.children[0].children[i].remove();
        }
    }

    qml_data = xmlSer.serializeToString(bg_collections);
    qml_filename = "collections.xml";
    document.getElementById("page_req").innerHTML = "update";

    timeout_id = setTimeout(update_collections, 250);
}


/**
 * Отрисовка новой логической страницы<br/>
 * ('Список определённых существ'('List of items'))<br/>
 * @param {string} col_num - номер коллекции (по счёту) в bg_collections
*/
function bg_items_list(col_num) {
    if (!isNaN(col_num)) {
        bg_collection_num = Number(col_num);
    }
    bg_collection_title = bg_collections.children[0].children[bg_collection_num - 1].children[0].innerHTML;
    let p_bot = loadXMLtext("bg_collection_items_bot.html");

    document.getElementById("page_top").children[0].innerHTML = bg_collection_title;
    document.getElementById("page_bot").innerHTML = p_bot;

    xsltProcessor.setParameter("", "action", "show_collection_items_list");
    xsltProcessor.setParameter(null, "collection_num", bg_collection_num);

    let fragment = xsltProcessor.transformToFragment(bg_collections, document);

    document.getElementById("page_mid").textContent = "";
    document.getElementById("page_mid").appendChild(fragment);
}



/**
 * Обрабатывает запрос (OnClick) на удаление объекта:<br/>
 * обновление содержимого bg_collections (DOM-объект со списком коллекций)<br/>
 * перерисовка страницы<br/>
 * передача запроса на обновление collections.xml в qml<br/>
 * запуск таймера, по окончании кторого вызывается update_items() (проверка ответа)<br/>
*/
function bg_item_remove() {
    let i, count = 0, confirm;

    for (i = 0; i < bg_collections.children[0].children[bg_collection_num - 1].children[1].childElementCount; i++) {
        if (document.forms['items'].elements[i].checked != "") {
            count++;
        }
    }
    if (count == 0) {
        return;
    }

    confirm = window.confirm("Удалить выбранные объекты (" + count + ")?");

    if (confirm == 0) {
        return;
    }

    document.getElementById("sp_label").innerHTML = "Удаление...";
    document.getElementById("spinner").style.visibility = "visible";

    for (i = bg_collections.children[0].children[bg_collection_num - 1].children[1].childElementCount - 1; i >= 0; i--) {
        if (document.forms['items'].elements[i].checked != "") {
            bg_collections.children[0].children[bg_collection_num - 1].children[1].children[i].remove();
        }
    }

    qml_data = xmlSer.serializeToString(bg_collections);
    qml_filename = "collections.xml";
    document.getElementById("page_req").innerHTML = "update";

    timeout_id = setTimeout(update_items, 250);
}


/**
 * проверка и обработка ответа на запрос в qml на обновление коллекций<br/>
 * (при вызове по окончании таймера в функции bg_item_remove())<br/>
*/
function update_items() {
    if (document.getElementById("page_req").innerHTML != "") {
        timeout_id = setTimeout(update_items, 250);
        return;
    }

    document.getElementById("spinner").style.visibility = "hidden";

    bg_items_list();
}


/**
 * Отрисовка новой логической страницы<br/>
 * ('О приложении')<br/>
*/
function bg_about() {
    let p_mid = loadXMLtext("bg_about_mid.html");
    let p_bot = loadXMLtext("bg_about_bot.html");

    document.getElementById("page_bot").innerHTML = p_bot;
    document.getElementById("page_mid").innerHTML = p_mid;
}


/**
 * Изменение цвета фона блочного элемента (<div>)
*/
function highlight(tag, action) {
    if (action == 'on') {
        tag.style.background = "rgb(220, 220, 250)";
    } else {
        tag.style.background = "rgb(249, 235, 193)";
    }

}


/**
 * подготавливает глобальные переменные для дальнейшего использования<br/>
 * (в функциях работы с определителем)<br/>
 * вызывает bg_guide_show()
*/
function bg_guide_setup(filename, title) {
    bg_file = filename;
    bg_guide = loadXML(bg_file);
    bg_title = title;

    bg_guide_show();
}


/**
 * Отрисовка новой логической страницы<br/>
 * ('Информация об определителе')<br/>
*/
function bg_guide_show() {
    let p_bot = loadXMLtext("bg_guide_bot.html");

    document.getElementById("page_top").children[0].innerHTML = bg_title;
    document.getElementById("page_bot").innerHTML = p_bot;

    xsltProcessor.setParameter("", "action", "show_guide_info");

    let fragment = xsltProcessor.transformToFragment(bg_guide, document);

    document.getElementById("page_mid").textContent = "";
    document.getElementById("page_mid").appendChild(fragment);
}


/**
 * Отрисовка новой логической страницы<br/>
 * ('Список таблиц')<br/>
*/
function bg_tablelist() {
    let p_bot = loadXMLtext("bg_tablelist_bot.html");

    document.getElementById("page_bot").innerHTML = p_bot;
    document.getElementById("page_top").children[0].innerHTML = bg_title + " - Таблицы";

    xsltProcessor.setParameter(null, "action", "show_table_list");

    let fragment = xsltProcessor.transformToFragment(bg_guide, document);

    document.getElementById("page_mid").textContent = "";
    document.getElementById("page_mid").appendChild(fragment);
}


/**
 * подготавливает глобальные переменные и массивы для дальнейшего использования<br/>
 * (в функциях работы с тезами определителя)<br/>
 * Вызывает bg_determinate_show()
 * @param {string} taxon - название таксономического ранга
*/
function bg_determinate_prepare(taxon) {
    bg_history = [];
    bg_history.push([taxon, 1]);
    bg_history_index = bg_history.length - 1;

    bg_determinate_show();
}


/**
 * Отрисовка новой логической страницы<br/>
 * ('Определение вида')<br/>
*/
function bg_determinate_show() {
    let xpath = xpathEvaluator.createExpression('/guide/tables/taxon[@id = "' + bg_history[bg_history_index][0] + '"]/table/record[@id = "' + bg_history[bg_history_index][1] + '"]/@else');
    let otherwise = xpath.evaluate(bg_guide, XPathResult.NUMBER_TYPE);

    xpath = xpathEvaluator.createExpression('/guide/tables/taxon[@id = "' + bg_history[bg_history_index][0] + '"]/table/record[@id = "' + bg_history[bg_history_index][1] + '"]/@link');
    let link = xpath.evaluate(bg_guide, XPathResult.STRING_TYPE);

    xpath = xpathEvaluator.createExpression('count(/guide/tables/taxon[@id = "' + bg_history[bg_history_index][0] + '"]/description)');
    let n_descr = xpath.evaluate(bg_guide, XPathResult.NUMBER_TYPE);

    let p_bot = loadXMLtext("bg_determinate_bot.html");

    document.getElementById("page_bot").innerHTML = p_bot;

    if (n_descr.numberValue == 0) {
        document.getElementById("btn_info").setAttribute("disabled", "disabled");
    }

    if (bg_history_index == 0) {
        document.getElementById("btn_back").setAttribute("disabled", "disabled");
    }

    if (bg_history_index == bg_history.length - 1) {
        document.getElementById("btn_forward").setAttribute("disabled", "disabled");
    }

    if (otherwise.numberValue == 0) {
        document.getElementById("btn_no").setAttribute("disabled", "disabled");
    }

    xsltProcessor.setParameter(null, "action", "get_taxon_title");
    xsltProcessor.setParameter(null, "taxon_id", bg_history[bg_history_index][0]);

    let fragment = xsltProcessor.transformToFragment(bg_guide, document);
    let title = fragment.textContent;

    document.getElementById("page_top").children[0].innerHTML = title;

    xsltProcessor.setParameter(null, "action", "show_taxon_record");
    xsltProcessor.setParameter(null, "taxon_id", bg_history[bg_history_index][0]);
    xsltProcessor.setParameter(null, "record_num", bg_history[bg_history_index][1]);

    fragment = xsltProcessor.transformToFragment(bg_guide, document);

    document.getElementById("page_mid").textContent = "";
    document.getElementById("page_mid").appendChild(fragment);
}


/**
 * Отрисовка новой логической страницы<br/>
 * ('Конец определения')<br/>
*/
function bg_determinate_finish() {
    let p_bot = loadXMLtext("bg_determinate_finish_bot.html");

    document.getElementById("page_bot").innerHTML = p_bot;

    xsltProcessor.setParameter(null, "action", "show_record_specie");
    xsltProcessor.setParameter(null, "taxon_id", bg_history[bg_history_index][0]);
    xsltProcessor.setParameter(null, "record_num", bg_history[bg_history_index][1]);

    fragment = xsltProcessor.transformToFragment(bg_guide, document);

    document.getElementById("page_mid").textContent = "";
    document.getElementById("page_mid").appendChild(fragment);
}


/**
 * Выполняет переход по истории определения на 1 шаг назад<br/>
 * и вызывает bg_determinate_show() для перерисовки страницы<br/>
*/
function bg_determinate_back() {
    bg_history_index = bg_history_index - 1;
    bg_determinate_show();
}


/**
 * Выполняет переход по истории определения на 1 шаг вперёд<br/>
 * и вызывает bg_determinate_show() для перерисовки страницы<br/>
*/
function bg_determinate_forward() {
    bg_history_index = bg_history_index + 1;
    bg_determinate_show();
}


/**
 * Выполняет переход к следующей тезе при соглашении с текущей(<button id='btn_yes' onClick='bg_determinate_yes()'>)<br/>
 * и вызывает bg_determinate_show() или bg_determinate_finish() для перерисовки страницы<br/>
*/
function bg_determinate_yes() {
    bg_history.splice(bg_history_index + 1);

    let xpath = xpathEvaluator.createExpression('/guide/tables/taxon[@id = "' + bg_history[bg_history_index][0] + '"]/table/record[@id = "' + bg_history[bg_history_index][1] + '"]/@link');
    let link = xpath.evaluate(bg_guide, XPathResult.STRING_TYPE);

    xpath = xpathEvaluator.createExpression('count(/guide/tables/taxon[@id = "' + bg_history[bg_history_index][0] + '"]/table/record[@id = "' + bg_history[bg_history_index][1] + '"]/specie)');
    let n_specie = xpath.evaluate(bg_guide, XPathResult.NUMBER_TYPE);

    if (n_specie.numberValue == 1) {
        bg_determinate_finish();
    } else {
        if (link.stringValue == "") {
            bg_history.push([bg_history[bg_history_index][0], Number(bg_history[bg_history_index][1]) + 1]);
        } else {
            xpath = xpathEvaluator.createExpression('/guide/tables/taxon[@id = "' + link.stringValue + '"]/@link');
            let taxon_link = xpath.evaluate(bg_guide, XPathResult.STRING_TYPE);
            while (taxon_link.stringValue != "") {
                link = taxon_link;
                xpath = xpathEvaluator.createExpression('/guide/tables/taxon[@id = "' + taxon_link.stringValue + '"]/@link');
                taxon_link = xpath.evaluate(bg_guide, XPathResult.STRING_TYPE);
            }
            xpath = xpathEvaluator.createExpression('count(/guide/tables/taxon[@id = "' + link.stringValue + '"]/table)');
            let n_table = xpath.evaluate(bg_guide, XPathResult.NUMBER_TYPE);
            if (n_table.numberValue == 0) {
                bg_history.push([link.stringValue, 0]);
                bg_history_index = bg_history.length - 1;
                bg_determinate_finish();
                return;
            }
            bg_history.push([link.stringValue, 1]);
        }
        bg_history_index = bg_history.length - 1;
        bg_determinate_show();
    }
}


/**
 * Выполняет переход к следующей тезе при несогласии с текущей(<button id='btn_no' onClick='bg_determinate_yes()'>)<br/>
 * и вызывает bg_determinate_show() для перерисовки страницы<br/>
*/
function bg_determinate_no() {
    bg_history.splice(bg_history_index + 1);

    let xpath = xpathEvaluator.createExpression('/guide/tables/taxon[@id = "' + bg_history[bg_history_index][0] + '"]/table/record[@id = "' + bg_history[bg_history_index][1] + '"]/@else');
    let otherwise = xpath.evaluate(bg_guide, XPathResult.NUMBER_TYPE);

    bg_history.push([bg_history[bg_history_index][0], otherwise.numberValue]);
    bg_history_index = bg_history.length - 1;

    bg_determinate_show();
}


/**
 * Отрисовка новой логической страницы<br/>
 * ('Информация о таксоне')<br/>
*/
function bg_determinate_taxon_description() {
    let p_bot = loadXMLtext("bg_determinate_taxon_description_bot.html");

    document.getElementById("page_bot").innerHTML = p_bot;

    xsltProcessor.setParameter(null, "action", "show_taxon_description");
    xsltProcessor.setParameter(null, "taxon_id", bg_history[bg_history_index][0]);

    let fragment = xsltProcessor.transformToFragment(bg_guide, document);

    document.getElementById("page_mid").textContent = "";

    document.getElementById("page_mid").appendChild(fragment);
}


/**
 * Отрисовка новой логической страницы<br/>
 * ('Сохранение определённого объекта')<br/>
*/
function bg_determinate_save() {
    xsltProcessor.setParameter(null, "action", "get_specie_title");
    xsltProcessor.setParameter(null, "taxon_id", bg_history[bg_history.length - 1][0]);
    xsltProcessor.setParameter(null, "record_num", bg_history[bg_history.length - 1][1]);

    let fragment = xsltProcessor.transformToFragment(bg_guide, document);

    document.getElementById("page_top").children[0].innerHTML = fragment.textContent;

    xsltProcessor.setParameter(null, "action", "show_item_save");

    fragment = xsltProcessor.transformToFragment(bg_collections, document);

    document.getElementById("page_mid").textContent = "";
    document.getElementById("page_mid").appendChild(fragment);

    let p_bot = loadXMLtext("bg_determinate_save_bot.html");

    document.getElementById("page_bot").innerHTML = p_bot;
}

/**
 * Блокирует поле ввода названия новой коллекции<br/>
 * в зависимости от выбранного пункта списка коллекций<br/>
 * (на логической странице 'Сохранение определённого объекта')<br/>
 * @param {string} s - Название выбранного пункта списка коллекций
*/
function item_save_select_col(s) {
    if (s.value != "Новая коллекция") {
	document.getElementById("new_col").setAttribute("disabled", "disabled");
    } else {
	document.getElementById("new_col").removeAttribute("disabled");
    }
}


/**
 * Обрабатывает запрос (OnClick) на сохранение нового обекта:<br/>
 * создание нового (дополнительного) содержимого для collections.xml
 * обновление содержимого bg_collections (DOM-объект со списком коллекций)<br/>
 * передача запроса на обновление collections.xml в qml<br/>
 * запуск таймера, по окончании кторого вызывается update_collections2() (проверка ответа)<br/>
*/
function bg_determinate_save_item() {
    let xpath, parent, parent_id, item, i, collection;
    let xmlSer = new XMLSerializer();

    let col = document.getElementById("col_list").value;
    let new_col = document.getElementById("new_col").value;

    if (col == "Новая коллекция") {
        if (new_col == "") {
            window.alert("Необходимо указать имя коллекции");
            return;
        }

        for (i = 0; i < bg_collections.children[0].childElementCount; i++) {
            if (new_col == bg_collections.children[0].children[i]) {
                break;
            }
        }

        if (i == bg_collections.children[0].childElementCount) {
            collection = '\
<?xml version="1.0" encoding="utf-8"?>\n\
	<collection>\n\
	    <name>' + new_col + '</name>\n\
	    <items>\n\
	    </items>\n\
	</collection>';
            let doc = parser.parseFromString(collection, "application/xml");
            bg_collections.children[0].append(doc.children[0]);
        }
        col = new_col;
    }

    parent_id = bg_history[bg_history.length - 1][0];
    let taxon_id = [];

    while (parent_id != "") {
        taxon_id.unshift(parent_id);
        xpath = xpathEvaluator.createExpression('/guide/tables/taxon[@id = "' + parent_id + '"]/@parent');
        parent = xpath.evaluate(bg_guide, XPathResult.STRING_TYPE);
        parent_id = parent.stringValue;
    }

    event = new Date();

    item = '\
<?xml version="1.0" encoding="utf-8"?>\n\
		<item>\n\
		    <guide filename="' + bg_file + '">\n\
			<title>'+ bg_title + '</title>\n\
		    </guide>\n\
		    <description>\n\
			<collect>\n\
			    <name>' + document.getElementById("col_name").value + '</name>\n\
			    <place>' + document.getElementById("col_place").value + '</place>\n\
			    <date>' + document.getElementById("col_date").value + '</date>\n\
			    <comment>' + document.getElementById("col_comment").value + '</comment>\n\
			</collect>\n\
			<determine>\n\
			    <name>' + document.getElementById("det_name").value + '</name>\n\
			    <date>' + event.toISOString() + '</date>\n\
			</determine>\n\
		    </description>\n\
		    <taxonomy>\n';

    for (i = 0; i < taxon_id.length; i++) {
        xsltProcessor.setParameter(null, "action", "get_taxon_title");
        xsltProcessor.setParameter(null, "taxon_id", taxon_id[i]);

        fragment = xsltProcessor.transformToFragment(bg_guide, document);

        item = item + '\
			<taxon>\n\
			    <title>' + fragment.textContent + '</title>\n\
			</taxon>\n';
    }

    xsltProcessor.setParameter(null, "action", "get_specie_title");
    xsltProcessor.setParameter(null, "taxon_id", bg_history[bg_history.length - 1][0]);
    xsltProcessor.setParameter(null, "record_num", bg_history[bg_history.length - 1][1]);

    fragment = xsltProcessor.transformToFragment(bg_guide, document);

    if (fragment.textContent != "") {
        item = item + '\
			<taxon>\n\
			    <title>' + fragment.textContent + '</title>\n\
			</taxon>\n';
    }

    xsltProcessor.setParameter(null, "action", "get_subtaxon_titles");
    xsltProcessor.setParameter(null, "taxon_id", bg_history[bg_history.length - 1][0]);
    xsltProcessor.setParameter(null, "record_num", bg_history[bg_history.length - 1][1]);

    fragment = xsltProcessor.transformToFragment(bg_guide, document);

    for (i = 0; i < fragment.childElementCount; i++) {
        item = item + '\
			<taxon>\n\
			    <title>';
        item = item + '\n' + fragment.children[i].children[0].innerHTML + '\n';
        item = item + '\
			    </title>\n\
			</taxon>';
    }

    item = item + '\
		    </taxonomy>\n\
		</item>';

    doc = parser.parseFromString(item, "application/xml");

    for (i = 0; i < bg_collections.children[0].childElementCount; i++) {
        if (col == bg_collections.children[0].children[i].children[0].innerHTML) {
            bg_collections.children[0].children[i].children[1].append(doc.children[0]);
            break;
        }
    }

    qml_data = xmlSer.serializeToString(bg_collections);
    qml_filename = "collections.xml";
    document.getElementById("page_req").innerHTML = "update";

    timeout_id = setTimeout(update_collections2, 250);

//console.error(xmlSer.serializeToString(bg_collections));
}


/**
 * Отрисовка новой логической страницы<br/>
 * ('Информация об объекте')
 * @param {number} item_num - Порядковый номер обекта в списке (логическая страница 'List of items')<br/>
 * (совпадает с порядковым номером в collections.xml)
*/
function bg_item_info(item_num) {
    let xpath = xpathEvaluator.createExpression('/collections/collection[' + bg_collection_num + ']/items/item[' + item_num + ']/taxonomy/taxon[count(//taxonomy/taxon)]/title');
    let taxon = xpath.evaluate(bg_collections, XPathResult.STRING_TYPE);

    document.getElementById("page_top").children[0].innerHTML = taxon.stringValue;

    let p_bot = loadXMLtext("bg_collection_item_bot.html");

    document.getElementById("page_bot").innerHTML = p_bot;

    xsltProcessor.setParameter(null, "action", "show_item_info");
    xsltProcessor.setParameter(null, "collection_num", bg_collection_num);
    xsltProcessor.setParameter(null, "item_num", item_num);

    let fragment = xsltProcessor.transformToFragment(bg_collections, document);

    document.getElementById("page_mid").textContent = "";
    document.getElementById("page_mid").appendChild(fragment);
}
