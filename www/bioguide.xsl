<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="html"/>

<!-- название шаблона, который будет вызван из шаблона для корня-->
    <xsl:param name="action" select="''"/>

<!-- идентификатор таксона (латинское имя)-->
    <xsl:param name="taxon_id" select="''"/>

<!-- номер тезы в таксоне-->
    <xsl:param name="record_num" select="''"/>

<!-- порядковый номер коллекции в collections.xml-->
    <xsl:param name="collection_num" select="''"/>

<!-- порядковый номер объекта в коллекции в collections.xml-->
    <xsl:param name="item_num" select="''"/>

<!--
    осуществляет вызов других шаблонов, в зарисимости от значения глобального параметра 'action'
    (передаются из js)
    @xpath /
-->
    <xsl:template match="/">
	<xsl:choose>
	    <xsl:when test="$action = 'show_guide_list'">
		<xsl:call-template name="show_guide_list"/>
	    </xsl:when>
	    <xsl:when test="$action = 'show_guide_list_server'">
		<xsl:call-template name="show_guide_list_server"/>
	    </xsl:when>
	    <xsl:when test="$action = 'show_collections_list'">
		<xsl:call-template name="show_collections_list"/>
	    </xsl:when>
	    <xsl:when test="$action = 'show_collection_items_list'">
		<xsl:call-template name="show_collection_items_list"/>
	    </xsl:when>
	    <xsl:when test="$action = 'show_item_info'">
		<xsl:call-template name="show_item_info"/>
	    </xsl:when>
	    <xsl:when test="$action = 'show_guide_info'">
		<xsl:call-template name="show_guide_info"/>
	    </xsl:when>
	    <xsl:when test="$action = 'show_table_list'">
	        <xsl:call-template name="show_table_list"/>
	    </xsl:when>
	    <xsl:when test="$action = 'get_taxon_title'">
		<xsl:call-template name="get_taxon_title"/>
	    </xsl:when>
	    <xsl:when test="$action = 'get_subtaxon_titles'">
		<xsl:call-template name="get_subtaxon_titles"/>
	    </xsl:when>
	    <xsl:when test="$action = 'show_taxon_record'">
	        <xsl:call-template name="show_taxon_record"/>
	    </xsl:when>
	    <xsl:when test="$action = 'show_taxon_description'">
	        <xsl:call-template name="show_taxon_description"/>
	    </xsl:when>
	    <xsl:when test="$action = 'show_record_specie'">
	        <xsl:call-template name="show_record_specie"/>
	    </xsl:when>
	    <xsl:when test="$action = 'get_specie_title'">
	        <xsl:call-template name="get_specie_title"/>
	    </xsl:when>
	    <xsl:when test="$action = 'show_item_save'">
	        <xsl:call-template name="show_item_save"/>
	    </xsl:when>
	    <xsl:otherwise>
		<xsl:message terminate="yes">Bad action supplied</xsl:message>
	    </xsl:otherwise>
	</xsl:choose>
    </xsl:template>


<!--
    @output список загруженных определителей в виде html DOM-объекта
    (логическая страница 'Список определителей')
-->
    <xsl:template name="show_guide_list">
	<form id="guides" style="margin-bottom: 0;"></form>
	<xsl:for-each select="guides/guide">
	    <div style="display: flex; height: 15%; width: 100%; border-bottom: 1px solid #b0b0b0;" onMouseOver="highlight(this, 'on')" onMouseOut="highlight(this, 'off')">
		<div style="display: inline-flex; height: 100%; width: 5%; align-items: center;">
		    <input type="checkbox" form="guides" style="display: flex; margin-top: 0; margin-bottom: 0; margin-right: 20;"/>
		</div>
		<div style="display: inline-flex; height: 100%; width: 95%; align-items: center; cursor: pointer" onClick="bg_guide_setup('{@file}', '{title}')">
		    <div style="display: inline-flex; height: 100%; width: 5%; align-items: center;">
			<xsl:choose>
			    <xsl:when test="count(image) = 1">
				<img style="max-height: 100%; max-width: 100%" src="data:image/{image/@type};base64,{image}" border="1"/>
			    </xsl:when>
			    <xsl:otherwise>
				<img style="max-height: 100%; max-width: 100%" src="images/book.png" border="1"/>
			    </xsl:otherwise>
			</xsl:choose>
		    </div>
		    <div style="display: inline-flex; height: 100%; align-items: center; margin-left: 10;">
			<h3 style="margin: 0;">
			    <xsl:value-of select="title"/>
			</h3>
		    </div>
		</div>
	    </div>
	</xsl:for-each>
	<br/>
	<br/>
	<br/>
    </xsl:template>


<!--
    @output список определителей на сервере в виде html DOM-объекта
    (логическая страница 'Добавление определителей')
-->
    <xsl:template name="show_guide_list_server">
	<form id="guides_server" style="margin-bottom: 0;"></form>
	<xsl:for-each select="union/guides[2]/guide">
	    <xsl:variable name="server_file" select="@file"/>
	    <xsl:variable name="server_file_hash" select="hash"/>
	    <xsl:choose>
		<xsl:when test="count(/union/guides[1]/guide[@file = $server_file]) = 1">
		    <xsl:if test="string(/union/guides[1]/guide[@file = $server_file]/hash) != $server_file_hash">
			<div style="display: flex; height: 15%; width: 100%; border-bottom: 1px solid #b0b0b0;" onMouseOver="highlight(this, 'on')" onMouseOut="highlight(this, 'off')">
			    <div style="display: inline-flex; height: 100%; width: 5%; align-items: center;">
				<input value="{@file}" type="checkbox" form="guides_server" style="display: flex; margin-top: 0; margin-bottom: 0; margin-right: 20;"/>
				<img src="images/icons/update22_l.png"/>
			    </div>
			    <div style="display: inline-flex; height: 100%; width: 95%; align-items: center;">
				<div style="display: inline-flex; height: 100%; width: 5%; align-items: center;">
				    <xsl:choose>
					<xsl:when test="count(image) = 1">
					    <img style="max-height: 100%; max-width: 100%" src="data:image/{image/@type};base64,{image}" border="1"/>
					</xsl:when>
					<xsl:otherwise>
					    <img style="max-height: 100%; max-width: 100%" src="images/book.png" border="1"/>
					</xsl:otherwise>
				    </xsl:choose>
				</div>
				<div style="display: inline-flex; height: 100%; align-items: center; margin-left: 10;">
				    <h3 style="margin: 0;">
					<xsl:value-of select="title"/>
				    </h3>
				</div>
			    </div>
			</div>
		    </xsl:if>
		</xsl:when>
		<xsl:otherwise>
		    <div style="display: flex; height: 15%; width: 100%; border-bottom: 1px solid #b0b0b0;" onMouseOver="highlight(this, 'on')" onMouseOut="highlight(this, 'off')">
			<div style="display: inline-flex; height: 100%; width: 5%; align-items: center;">
			    <input value="{@file}" type="checkbox" form="guides_server" style="display: flex; margin-top: 0; margin-bottom: 0; margin-right: 20;"/>
			</div>
			<div style="display: inline-flex; height: 100%; width: 95%; align-items: center;">
			    <div style="display: inline-flex; height: 100%; width: 5%; align-items: center;">
				<xsl:choose>
				    <xsl:when test="count(image) = 1">
					<img style="max-height: 100%; max-width: 100%" src="data:image/{image/@type};base64,{image}" border="1"/>
				    </xsl:when>
				    <xsl:otherwise>
					<img style="max-height: 100%; max-width: 100%" src="images/book.png" border="1"/>
				    </xsl:otherwise>
				</xsl:choose>
			    </div>
			    <div style="display: inline-flex; height: 100%; align-items: center; margin-left: 10;">
				<h3 style="margin: 0;">
				    <xsl:value-of select="title"/>
				</h3>
			    </div>
			</div>
		    </div>
		</xsl:otherwise>
	    </xsl:choose>
	</xsl:for-each>
    </xsl:template>



<!--
    @output список коллекций в виде html DOM-объекта
    (логическая страница 'Список коллекций')
-->
    <xsl:template name="show_collections_list">
	<form id="collections" style="margin-bottom: 0;"></form>
	<xsl:for-each select="collections/collection">
	    <div style="display: flex; height: 10%; width: 100%; border-bottom: 1px solid #b0b0b0;" onMouseOver="highlight(this, 'on')" onMouseOut="highlight(this, 'off')">
		<div style="display: inline-flex; height: 100%; width: 5%; align-items: center;">
		    <input type="checkbox" form="collections" style="display: flex; margin-top: 0; margin-bottom: 0; margin-right: 20;"/>
		</div>
		<div style="display: inline-flex; height: 100%; width: 95%; align-items: center; cursor: pointer" onClick="bg_items_list('{position()}')">
		    <div style="display: inline-flex; height: 100%; align-items: center; margin-left: 10;">
			<h3 style="margin: 0;">
			    <xsl:value-of select="name"/>
			</h3>
		    </div>
		</div>
	    </div>
	</xsl:for-each>
    </xsl:template>


<!--
    @output список объектов одной коллекции в виде html DOM-объекта
    (логическая страница 'Список определённых существ'('List of items'))
-->
    <xsl:template name="show_collection_items_list">
	<form id="items" style="margin-bottom: 0;"></form>
	<xsl:for-each select="collections/collection[number($collection_num)]/items/item">
	    <div style="display: flex; height: 10%; width: 100%; border-bottom: 1px solid #b0b0b0;" onMouseOver="highlight(this, 'on')" onMouseOut="highlight(this, 'off')">
		<div style="display: inline-flex; height: 100%; width: 5%; align-items: center;">
		    <input type="checkbox" form="items" style="display: flex; margin-top: 0; margin-bottom: 0; margin-right: 20;"/>
		</div>
		<div style="display: inline-flex; height: 100%; width: 95%; align-items: center; cursor: pointer" onClick="bg_item_info('{position()}')">
		    <div style="display: inline-flex; height: 100%; align-items: center; margin-left: 10;">
			<h3 style="margin: 0;">
			    <xsl:variable name="t" select="count(taxonomy/taxon)"/>
			    <xsl:value-of select="taxonomy/taxon[$t]/title"/>
			</h3>
		    </div>
		</div>
	    </div>
	</xsl:for-each>
    </xsl:template>


<!--
    @output информацию об определённом обекте в виде html DOM-объекта
    (логическая страница 'Информация об объекте')
-->
    <xsl:template name="show_item_info">
	<div style="display: flex; height: 100%; width: 100%;">
	    <div style="display: flex; flex-direction: column; height: 100%; width: 50%;">
		<div style="display: inline-flex; width: 100%; max-height: 90%; margin-bottom: 5px">
		    <div style="width: 50%; height: 100%; vertical-align: top;">
			<p style="margin: 0">Определитель:</p>
		    </div>
		    <div style="width: 50%; height: 100%; vertical-align: top;">
			<xsl:value-of select="collections/collection[number($collection_num)]/items/item[number($item_num)]/guide/title"/>
		    </div>
		</div>
		<div style="display: inline-flex; width: 100%; max-height: 20%; margin-bottom: 5px">
		    <div style="width: 50%; height: 100%; vertical-align: top;">
			<p style="margin: 0">Собрал:</p>
		    </div>
		    <div style="width: 50%; height: 100%; vertical-align: top;">
			<xsl:value-of select="collections/collection[number($collection_num)]/items/item[number($item_num)]/description/collect/name"/>
		    </div>
		</div>
		<div style="display: inline-flex; width: 100%; max-height: 20%; margin-bottom: 5px">
		    <div style="width: 50%; height: 100%; vertical-align: top;">
			<p style="margin: 0">Место сбора:</p>
		    </div>
		    <div style="width: 50%; height: 100%; vertical-align: top;">
			<xsl:value-of select="collections/collection[number($collection_num)]/items/item[number($item_num)]/description/collect/place"/>
		    </div>
		</div>
		<div style="display: inline-flex; width: 100%; max-height: 20%; margin-bottom: 5px">
		    <div style="width: 50%; height: 100%; vertical-align: top;">
			<p style="margin: 0">Дата сбора:</p>
		    </div>
		    <div style="width: 50%; height: 100%; vertical-align: top;">
			<xsl:value-of select="collections/collection[number($collection_num)]/items/item[number($item_num)]/description/collect/date"/>
		    </div>
		</div>
		<div style="display: inline-flex; width: 100%; max-height: 90%; margin-bottom: 5px">
		    <div style="width: 50%; height: 100%; vertical-align: top;">
			<p style="margin: 0">Комментарий:</p>
		    </div>
		    <div style="width: 50%; height: 100%; vertical-align: top;">
			<xsl:value-of select="collections/collection[number($collection_num)]/items/item[number($item_num)]/description/collect/comment"/>
		    </div>
		</div>
		<div style="display: inline-flex; width: 100%; max-height: 20%; margin-bottom: 5px">
		    <div style="width: 50%; height: 100%; vertical-align: top;">
			<p style="margin: 0">Определил:</p>
		    </div>
		    <div style="width: 50%; height: 100%; vertical-align: top;">
			<xsl:value-of select="collections/collection[number($collection_num)]/items/item[number($item_num)]/description/determine/name"/>
		    </div>
		</div>
		<div style="display: inline-flex; width: 100%; max-height: 90%;">
		    <div style="width: 50%; height: 100%; vertical-align: top;">
			<p style="margin: 0">Дата определения:</p>
		    </div>
		    <div style="width: 50%; height: 100%; vertical-align: top;">
			<xsl:value-of select="collections/collection[number($collection_num)]/items/item[number($item_num)]/description/determine/date"/>
		    </div>
		</div>
	    </div>
	    <div style="height: 100%; width: 50%; margin-left: 10px; margin-top: 10px">
		<h2 align="center">Таксономия</h2>
		<xsl:for-each select="collections/collection[number($collection_num)]/items/item[number($item_num)]/taxonomy/taxon">
		    <h3><xsl:value-of select="title"/></h3>
		</xsl:for-each>
	    </div>
	</div>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
    </xsl:template>


<!--
    формирует часть страницы с информацией об определителе
    @xpath author
    @output имя автора определителя и запятую (не всегда)
-->
    <xsl:template match="author">
	<xsl:value-of select="."/>
	<xsl:if test="position() != last()">
	    <xsl:text>, </xsl:text>
	</xsl:if>
    </xsl:template>


<!--
    формирует часть страницы с информацией об определителе
    @xpath guide/authors
    @output слово 'Автор:' или 'Авторы:'
-->
    <xsl:template match="guide/authors">
	<p>
	    <xsl:if test="count(author) > 1">
		<b>Авторы: </b>
	    </xsl:if>
	    <xsl:if test="count(author) = 1">
		<b>Автор: </b>
	    </xsl:if>
	    <xsl:apply-templates select="author"/>
	</p>
    </xsl:template>


<!--
    формирует часть страницы с информацией об определителе
    @xpath guide/publishers
    @output информацию об издательстве
-->
    <xsl:template match="guide/publishers">
	<xsl:for-each select="publisher">
	    <p>
		<b>Издательство: </b>
		<xsl:value-of select="name"/>, <xsl:value-of select="place"/>, <xsl:value-of select="year"/>
	    </p>
	</xsl:for-each>
    </xsl:template>


<!--
    формирует часть страницы с информацией об определителе
    @xpath guide/profile
    @output профиль определителя (для кого/чего он нужен)
-->
    <xsl:template match="guide/profile">
	<p>
	    <b>Профиль: </b>
	    <xsl:value-of select="."/>
	</p>
    </xsl:template>


<!--
    формирует часть страницы с информацией об определителе
    вызывает шаблон для para
    @xpath guide/introduction/foreword
    @output введение
-->
    <xsl:template match="guide/introduction/foreword">
	<p align="center"><b>Введение</b></p>
	<xsl:apply-templates select="para"/>
    </xsl:template>


<!--
    вызывает шаблоны для text и picture
    @xpath para
-->
    <xsl:template match="para">
	<xsl:for-each select="text|picture">
	    <xsl:apply-templates select="."/>
	</xsl:for-each>
    </xsl:template>


<!--
    @xpath text
    @output текст
-->
    <xsl:template match="text">
	<xsl:value-of select="." disable-output-escaping="yes"/>
    </xsl:template>


<!--
    @xpath picture
    @output изображение с подписью к нему
-->
    <xsl:template match="picture">
	<xsl:variable name="pic_num" select="@imgid"/>
	<xsl:variable name="pic_path" select="/guide/images/image[@id = $pic_num]"/>
	<div align="center">
	    <img id="pic{$pic_num}" title="{$pic_path/title}" width="60%" border="1" src="data:image/{$pic_path/@type};base64,{$pic_path/data}"/>
            <div align="left" style="width: 60%; margin:0;">
                <i>Рис. <xsl:value-of select="$pic_num"/>. <xsl:value-of select="$pic_path/title"/>.</i>
            </div>
	</div>
        <xsl:if test="count($pic_path/description) = 1">
            <div align="center">
                <div align="left" style="width: 60%;"><font size="-1"><xsl:value-of select="$pic_path/description" disable-output-escaping="yes"/></font>
                </div>
            </div>
        </xsl:if>
        <br/>
    </xsl:template>


<!--
    выводит название главы и вызывает шаблон для para
    @xpath guide/introduction/chapter
    @output название главы
-->
    <xsl:template match="guide/introduction/chapter">
	<p align="center"><b>Глава <xsl:value-of select="position()"/>. <xsl:value-of select="title"/></b></p>
	<xsl:apply-templates select="para"/>
    </xsl:template>


<!--
    @output информацию об определителе в виде html DOM-объекта
    (логическая страница 'Информация об определителе')
-->
    <xsl:template name="show_guide_info">
	<div style="display: flex; width: 100%">
	    <div style="display: inline-flex; height: 100%; width: 20%; align-items: center; justify-content: center; margin-left: 10px; padding-top: 5px;">
		<xsl:choose>
		    <xsl:when test="count(guide/title/@imgid) = 1">
			<xsl:variable name="cover" select="guide/title/@imgid"/>
			<img style="max-height: 100%; max-width: 100%" src="data:image/{guide/images/image[@id=$cover]/@type};base64,{guide/images/image[@id=$cover]/data}" border="1"/>
		    </xsl:when>
		    <xsl:otherwise>
			<img style="max-height: 100%; max-width: 100%" src="images/book.png"/>
		    </xsl:otherwise>
		</xsl:choose>
	    </div>

	    <div style="height: 100%; max-width: 80%; margin-left: 10px; align-items: center;">
		<xsl:apply-templates select="guide/authors"/>
		<xsl:if test="count(guide/authors) = 0">
		    <p>
			<b>Автор: </b><i>нет данных</i>
		    </p>
		</xsl:if>
		<xsl:apply-templates select="guide/publishers"/>
		<xsl:if test="count(guide/publishers) = 0">
		    <p>
			<b>Издательство: </b><i>нет данных</i>
		    </p>
		</xsl:if>
		<xsl:apply-templates select="guide/profile"/>
		<xsl:if test="count(guide/profile) = 0">
			<p>
			    <b>Профиль: </b><i>нет данных</i>
			</p>
		</xsl:if>
		<div>
		    <xsl:value-of select="guide/annotation/text()" disable-output-escaping="yes"/>
		</div>
	    </div>
	</div>

	<div>
	    <hr/>
	</div>

	<div>
	    <xsl:apply-templates select="guide/introduction/foreword"/>
	    <xsl:apply-templates select="guide/introduction/chapter"/>
	</div>
	<br/>
	<br/>
	<br/>
    </xsl:template>


<!--
    @xpath latin/name
    @mode a
    @output латинское название таксона в скобках
-->
    <xsl:template match="latin/name" mode="a">
	<xsl:if test="position() = 1">
	    <xsl:text> (</xsl:text>
	</xsl:if>
	<xsl:value-of select="."/>
	<xsl:if test="position() != last()">
	    <xsl:text>, или </xsl:text>
	</xsl:if>
	<xsl:if test="position() = last()">
	    <xsl:text>)</xsl:text>
	</xsl:if>
    </xsl:template>


<!--
    @xpath latin/name
    @mode b
    @output латинское название таксона
-->
    <xsl:template match="latin/name" mode="b">
	<xsl:value-of select="."/>
	<xsl:if test="position() != last()">
	    <xsl:text>, или </xsl:text>
	</xsl:if>
    </xsl:template>


<!--
    вызывает шаблон output_taxon_title
    @xpath guide/tables/taxon
    @mode get_title
-->
    <xsl:template match="guide/tables/taxon" mode="get_title">
	<xsl:call-template name="output_taxon_title"/>
    </xsl:template>


<!--
    вызывает шаблон output_taxon_title
    @xpath guide/tables/taxon/table/record/specie/taxonomy/taxon
    @mode get_title
-->
    <xsl:template match="guide/tables/taxon/table/record/specie/taxonomy/taxon" mode="get_title">
	<taxon>
	    <title>
		<xsl:call-template name="output_taxon_title"/>
	    </title>
	</taxon>
    </xsl:template>


<!--
    выводит таксон, используя шаблон для latin/name
    @output  и название таксона (для списка таблиц)
-->
    <xsl:template name="output_taxon_title">
	<xsl:value-of select="type"/>
	<xsl:text> </xsl:text>
	<xsl:choose>
	    <xsl:when test="count(name) = 1">
	        <xsl:value-of select="name"/>
	        <xsl:apply-templates select="latin/name" mode="a"/>
	    </xsl:when>
	    <xsl:otherwise>
	        <xsl:apply-templates select="latin/name" mode="b"/>
	    </xsl:otherwise>
	</xsl:choose>
    </xsl:template>


<!--
    вызывает шаблон output_taxon_title
    @output список таблиц в виде html DOM-объекта
    (логическая страница 'Список таблиц')
-->
    <xsl:template name="show_table_list">
	<xsl:for-each select="guide/tables/taxon[@list = '1']">
	    <div style="display: flex; min-height: 10%; width: 100%; border-bottom: 1px solid #b0b0b0; cursor: pointer; align-items: center;" onClick="bg_determinate_prepare('{@id}')" onMouseOver="highlight(this, 'on')" onMouseOut="highlight(this, 'off')">
		<div style="margin-left: 10">
		    <h4 style="margin: 0;">
			<xsl:call-template name="output_taxon_title"/>
		    </h4>
		</div>
	    </div>
	</xsl:for-each>
	<br/>
	<br/>
	<br/>

    </xsl:template>


<!--
    вызывает шаблон для guide/tables/taxon с mode get_title
-->
    <xsl:template name="get_taxon_title">
	<xsl:apply-templates select="guide/tables/taxon[@id = $taxon_id]" mode="get_title"/>
    </xsl:template>


<!--
    вызывает шаблон для guide/tables/taxon/record/specie/taxonomy/taxon с mode get_title
-->
    <xsl:template name="get_subtaxon_titles">
	<xsl:apply-templates select="guide/tables/taxon[@id = $taxon_id]/table/record[@id = $record_num]/specie/taxonomy/taxon" mode="get_title"/>
    </xsl:template>


<!--
    вызывает шаблон для guide/tables/taxon/table/record/thesis/para
    @output тезу определителя в виде html DOM-объекта
    (логическая страница 'Определение вида')
-->
    <xsl:template name="show_taxon_record">
	<div style="display: flex; width: 100%; min-height: 100%; justify-content: center; align-items: center;">
	    <div style="width: 60%;">
		<xsl:apply-templates select="guide/tables/taxon[@id = $taxon_id]/table/record[@id = $record_num]/thesis/para"/>
	    </div>
	</div>
	<br/>
	<br/>
	<br/>
    </xsl:template>


<!--
    вызывает шаблон для guide/tables/taxon/description/para
    @output информацию о таксоне в виде html DOM-объекта
    (логическая страница 'Информация о таксоне')
-->
    <xsl:template name="show_taxon_description">
	<div style="display: flex; width: 100%; min-height: 100%; justify-content: center; align-items: center;">
	    <div style="width: 60%;">
		<xsl:apply-templates select="guide/tables/taxon[@id = $taxon_id]/description/para"/>
	    </div>
	</div>
	<br/>
	<br/>
	<br/>
    </xsl:template>


<!--
    промежуточный шаблон
    вызывает шаблон output_taxon_title
    @xpath guide/tables/taxon/table/record/specie
    @mode get_title
-->
    <xsl:template match="guide/tables/taxon/table/record/specie" mode="get_title">
	<xsl:call-template name="output_taxon_title"/>
    </xsl:template>


<!--
    вызывает шаблон для guide/tables/taxon/table/record/specie с mode get_title
-->
    <xsl:template name="get_specie_title">
	<xsl:apply-templates select="guide/tables/taxon[@id = $taxon_id]/table/record[@id = $record_num]/specie" mode="get_title"/>
    </xsl:template>


<!--
    вызывает шаблон для guide/tables/taxon/table/record/description/para
    и guide/tables/taxon/table/record/specie
    @output вид и информацию о нём в виде html DOM-объекта
    (логическая страница 'Конец определения')
-->
    <xsl:template name="show_record_specie">
	<div style="display: flex; width: 100%; min-height: 100%; justify-content: center; align-items: center;">
	    <div style="width: 60%;">
		<p>
		    <xsl:apply-templates select="guide/tables/taxon[@id = $taxon_id]/table/record[@id = $record_num]/specie" mode="get_title"/>
		</p>
		<xsl:apply-templates select="guide/tables/taxon[@id = $taxon_id]/table/record[@id = $record_num]/specie/description/para"/>
		<xsl:if test="count(guide/tables/taxon[@id = $taxon_id]/table/record[@id = $record_num]/specie) = 0">
		    <xsl:apply-templates select="guide/tables/taxon[@id = $taxon_id]"/>
		</xsl:if>
	    </div>
	</div>
	<br/>
	<br/>
	<br/>
    </xsl:template>


<!--
    @output форму для заполнения информации об определяемом объекте в виде html DOM-объекта
    (логическая страница 'Сохранение определённого объекта')
-->
    <xsl:template name="show_item_save">
	<div style="display: flex; height: 100%; width: 100%; justify-content: center;">
	    <div style="height: 100%; width: 90%;">
		<div style="float: left; height: 100%; width: 40%;">
		    <br></br>
			<p style="margin: 0">Собрал:</p>
			<input id="col_name" type="text" size="15" maxlength="50" style="margin-bottom: 10px;"/><br></br>
			<p style="margin: 0">Место сбора:</p>
			<input id="col_place" type="text" size="15" maxlength="50" style="margin-bottom: 10px;"/><br></br>
			<p style="margin: 0">Дата сбора:</p>
			<input id="col_date" type="date" style="margin-bottom: 10px;"/><br></br>
			<p style="margin: 0">Комментарий:</p>
			<textarea id="col_comment" cols="25" rows="5" maxlength="250" style="margin-bottom: 10px;"></textarea><br></br>
			<p style="margin: 0">Определил:</p>
			<input id="det_name" type="text" size="15" maxlength="50"/><br></br>
		</div>
		<div style="display: inline-block; height: 100%; width: 60%;">
		    <br></br>
		    <p style="margin: 0">Выбор коллекции:</p>
		    <select id="col_list" size="1" onChange="item_save_select_col(this)">
			<option>Новая коллекция</option>

			<xsl:for-each select="collections/collection">
			    <option>
				<xsl:value-of select="name"/>
			    </option>
			</xsl:for-each>

		    </select>
		    <xsl:text>  </xsl:text>
		    <input id="new_col" type="text" size="15" maxlength="50"/><br></br>
		</div>
	    </div>
	</div>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
    </xsl:template>

</xsl:stylesheet>
