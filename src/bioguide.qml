import QtQuick 2.2
import QtQuick.Controls 1.1
import QtWebView 1.1
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.2


ApplicationWindow {
    visible: true
    width: initialWidth
    height: initialHeight


    WebView {
        id: webView
        anchors.fill: parent
        url: initialUrl
    }


    Timer {
        id: req_timer
        interval: 500
        running: true
        onTriggered: {
            let js = 'document.getElementById("page_req").textContent + "\\n-----\\n" + qml_filename + "\\n-----\\n" + qml_data';
            webView.runJavaScript(js, parse_request);
        }
    }


    function parse_request(request) {
        let req_parts = request.split("\n-----\n");
        if (req_parts[0] == "") {
            req_timer.start();
            return;
        }
        if (req_parts[0] == "remove") {
            fileio.remove(req_parts[1]);
        }
        if (req_parts[0] == "save") {
            fileio.write(req_parts[1], req_parts[2]);
        }
        if (req_parts[0] == "update") {
            fileio.write(req_parts[1] + ".new", req_parts[2]);
            fileio.remove(req_parts[1]);
            fileio.rename(req_parts[1] + ".new", req_parts[1]);
        }
        webView.runJavaScript('document.getElementById("page_req").innerHTML = ""');
        req_timer.start();
    }
}
