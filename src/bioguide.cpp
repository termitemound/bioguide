#include <QtCore/QUrl>
#include <QtCore/QCommandLineOption>
#include <QtCore/QCommandLineParser>
#include <QGuiApplication>
#include <QStyleHints>
#include <QScreen>
#include <QQmlApplicationEngine>
#include <QtQml/QQmlContext>
#include <QtWebView/QtWebView>
#include <QStandardPaths>
#include <iostream>
#include "fileio.h"


int main(int argc, char *argv[])
{
    int i;
    int newArgc = argc + 1;
    char ARG_DISABLE_WEB_SECURITY[] = "--disable-web-security";
    char **newArgv;

//! [0]
    QtWebView::initialize();

    newArgv = new char*[argc + 1];

    newArgv[argc] = ARG_DISABLE_WEB_SECURITY;

    for (i = 0; i < argc; i++) {
        newArgv[i] = argv[i];
    }

    QGuiApplication app(newArgc, newArgv);
//! [0]
    QGuiApplication::setApplicationDisplayName("BioGuide – определитель видов");

    app.setWindowIcon(QIcon(":/bioguide.png"));

    QString data_location_name = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    if (data_location_name.size() == 0) {
        return -1;
    }

    QDir data_location_dir = QDir(data_location_name);
    QString data_location_name_abs = data_location_dir.absolutePath();
    if (data_location_dir.mkpath(data_location_name_abs) == false) {
        return -1;
    }

    QDirIterator qrc_itr(QString(":/www"), QDir::Files, QDirIterator::Subdirectories);

    while (qrc_itr.hasNext() == true) {
        QString qrc_content_fname = qrc_itr.next();
        QString destination_fname = data_location_name_abs + qrc_content_fname.right(qrc_content_fname.size() - 1);
        QFileInfo destination_fname_info = QFileInfo(destination_fname);
        QDir destination_dir = destination_fname_info.dir();
        if (destination_dir.mkpath(destination_dir.absolutePath()) == false) {
            return -1;
        }
        QFile::copy(qrc_content_fname, destination_fname);
        QFile::setPermissions(destination_fname, QFileDevice::ReadOwner | QFileDevice::WriteOwner);
    }

    QString root_page = QStandardPaths::locate(QStandardPaths::AppDataLocation, QString("www/bioguide.html"), QStandardPaths::LocateFile);

    if (root_page.size() == 0) {
        return -1;
    }

    QQmlApplicationEngine engine;
    QQmlContext *context = engine.rootContext();
    context->setContextProperty(QStringLiteral("initialUrl"), QUrl::fromLocalFile(root_page));
    context->setContextProperty(QStringLiteral("fileio"), new FileIO(data_location_name_abs + QString("/www")));

    QRect geometry = QGuiApplication::primaryScreen()->availableGeometry();
    context->setContextProperty(QStringLiteral("initialWidth"), geometry.width());
    context->setContextProperty(QStringLiteral("initialHeight"), geometry.height());

    engine.load(QUrl(QStringLiteral("qrc:/bioguide.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
