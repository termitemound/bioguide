Summary: Biological guide system
Name:    bioguide
Version: 1.0.0
Release: 1%{?dist}

License: GPLv3 with exceptions
Url:     https://gitlab.com/termitemound/bioguide.git
Source0: %{name}-%{version}.tar.gz


BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-qtwebview-devel

# handled by qt5-srpm-macros, which defines %%qt5_qtwebengine_arches
%{?qt5_qtwebengine_arches:Requires: qt5-qtwebengine}

%description
bioguide is Qt5 based application attended to evaluate biological guides. bioguide
is offline web-application which requires no network connection and can be run at field conditions.


%prep
%autosetup -n %{name}-%{version} -p1


%build
%{qmake_qt5}

%make_build


%install
make install INSTALL_ROOT=%{buildroot}


%ldconfig_scriptlets

%files
%license LICENSE
%{_bindir}/%{name}
%{_datadir}/%{name}/*


%changelog
* Sat Apr 23 2022 Maria Shumakova <maria1407.sh@gmail.com> - 1.0.0-1
- Initial release
